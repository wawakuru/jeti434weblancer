# Requirements

The software we are trying to read data from is the Jungheinrich Jeti 4.34 (Located in `C:\Jeti\Program\Jungh.exe` on the virtual machine). It describes which parts are available for certain vehicles using exploded-views. We model this using two main entities, the path leading to a document and the document itself.

## Software

After you start the program, you are greeted with a tree like selection, where you specify which vehicle's parts you are interested in. (Meaning these are the paths nodes describing the vehicle) 
![main screen](jeti-434-tree.JPG)

Double clicking on the catalog takes you to a different selection, but it is still just the continuation of the tree nodes. These describe the parts itself, differentiating between a drive unit and a brake system for example. 

![product tree](jeti-434-tree3.JPG)

Some of the subgroups do not have further navigation, but most of them allows you to double click on them, which opens the document. The exploded-view has numbered references, which the rows in the table use to link the parts. A row in the table can also point to a different document (downward arrow).

![document](jeti-434-tree4.JPG)

The software allows the users to further filter the documents by manually entering a chassis number. This information should be available in the database and depending on how it is structured, should be provided as a separate dataset or part of the document's meta data.

![document](jeti-434-chassis1.JPG)

## Document

Contains one or more images, has a name and rows. (Optionally, if the chassis number referenced here, has a nullable chassisNumber reference). If one row is referencing another document, that one should be saved as well, and an extra field should be stored for it on the referencing row.

```json
{
    "docName": "Drive Traction / 80V...etc",
    "images": [
        "image reference string, or object"
    ],
    "rows": [
        {
            "refNumber": "number as string",
            "quantity": 1,
            "itemReference": "the part's reference number, like 51047023",
            "itemDescription": "Drive Unit",
            "documentReference": "optional document referencing"
        },
        ...etc
    ]
}
```

## Path

Each path node has a type and a value.  For example, looking at `ETH->ETH 20-50 P->50106066 Year 3.69-8.82->Electric Drive Unit-> 51047059 Drive Traction 80V/7,6KW`

```json
{
    "path": [
        {
            "value": "ETH",
            "type": "Product"
        },
        {
            "value": "ETH 20-50 P",
            "type": "Type"
        },
        {
            "value": "50106066 Year 3.69-8.82",
            "type": "Catalogue and Year"
        },
        {
           "value": "Electric Drive Unit",
           "type": "Main Group" 
        },
        {
            "value": "51047059 Drive Traction 80V/7,6KW",
            "type": "Subgroup"
        }
    ]
}
```

## Output

Even though I described the data in json format, You can provide the resultset as:

- MySQL Database
- MSSQL Database
- PostgreSQL Database
- SQLite Database
- MongoDB Database
- Or If you really want, flat Json files (each document is a seperate json file, containing its path and the document with it's rows)

All the images seem to be located in `C:\Jeti\Grafiken` folder, my guess is that the database stores the names or paths for it. The meta data should store the reference for the images, do not save them in the database.




